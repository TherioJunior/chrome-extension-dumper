# Chrome Extension Dumper

## Name
TherioJunior's Chrome Extension Dumper

## Description
This is a self-hostable privacy focused open source website written in pure HTML/CSS/PHP/JS without usage of any additional frameworks or libraries, with which you can download the *.crx file of Chrome Extensions, convert it into a proper *.zip file and then view its source code.

This is useful if you're like me conscious about your privacy and want to check the source code of extensions prior to installing them, as some extensions are malicious and could potentially steal your data and send it somewhere.

Converting the file into a proper *.zip file can sometimes be required depending on your archival application, as Chrome Extensions feature, as of the time of writing this, additional `1321` bytes of metadata at the start of the file, which can break file validation techniques implemented by some archival applications to ensure you're opening a proper *.zip file. The `unzip` Linux tool is not affected by this, it will simply skip those metadata bytes, but for example KDE's archival tool `Ark` is affected by this. The website offers inbuilt functionality for this.

## Features
- Dumping of Chrome Extensions in the base *.crx format.
- Automatic latest Chrome version fetching via `https://versionhistory.googleapis.com/v1/chrome/platforms/win/channels/stable/versions` to ensure you're dumping the latest extension version available for the latest browser stable release
- Button to open the Chrome Web Store to choose an extension you want to dump
- Inbuilt converting of *.crx files to *.zip files, reasons for which are explained in the [Description](#description) section.
- Privacy focused: No tracking whatsoever, completely open source and self-hostable
- Easy setup: No usage of additional frameworks or libraries
- Secure: Converter is secured against directory traversal and several other vulnerabilities
- Resource-light

## Visuals
![showcase.png](pictures%2Fshowcase.png)

## Installation
You can self-host this locally on your own PC or on a server of your choice by simply cloning this repository into the directory your installed web server serves files from. No additional dependencies or frameworks required, it's written in pure HTML/CSS/PHP/JS.

## Usage
Go to the Chrome Web Store via the on-site button, search for any extension of your choice, copy its link, paste it into the text box of the website, and then press the download button.

You may need to convert the downloaded *.crx file into a proper *.zip file because of additional metadata that is included in Chrome Extensions, for which the site has inbuilt functionality.

## Support
If you run into issues with this, just open an issue on this repository, and I'll fix it as soon as I can.

## Contributing
If you want any changes implemented clone this repository locally and open a merge request :)

## Authors and acknowledgment
[TherioJunior](https://gitlab.com/TherioJunior)

## License
GPL v3.0 - Use this for whatever you want, wherever you want, as long as your modifications to the code stay open source (not legal advice)
