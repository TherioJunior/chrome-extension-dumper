<?php
function processFile($file) {
    // Validate file upload
    if (!isset($file['error']) || is_array($file['error'])) {
        throw new RuntimeException('Invalid file upload.');
    }

    // Check for file upload errors
    switch ($file['error']) {
        case UPLOAD_ERR_OK:
            break;
        case UPLOAD_ERR_NO_FILE:
            throw new RuntimeException('No file sent.');
        case UPLOAD_ERR_INI_SIZE:
        case UPLOAD_ERR_FORM_SIZE:
            throw new RuntimeException('Exceeded filesize limit.');
        default:
            throw new RuntimeException('Unknown errors.');
    }

    // MIME type check for .crx files
    $finfo = new finfo(FILEINFO_MIME_TYPE);
    if ($finfo->file($file['tmp_name']) !== 'application/x-chrome-extension') {
        return ['error' => 'Error: File is not a valid .crx format.'];
    }

    $sourcePath = $file['tmp_name'];
    $destPath = 'processed_files/' . bin2hex(random_bytes(10)) . '.zip';

    // Ensure processed_files directory exists and is secure
    $processedFilesDir = 'processed_files';
    if (!is_dir($processedFilesDir)) {
        if (!mkdir($processedFilesDir, 0770, true)) {
            throw new RuntimeException('Failed to create processed_files directory.');
        }

        // Secure the directory
        file_put_contents($processedFilesDir . '/.htaccess', "deny from all");
        file_put_contents($processedFilesDir . '/index.html', '');
    }

    // Stream-based file reading and writing
    $handle = fopen($sourcePath, "rb");
    if (!$handle) {
        return ['error' => 'Error: Unable to open source file.'];
    }

    fseek($handle, 1321); // Skip first 1321 bytes

    $destHandle = fopen($destPath, 'wb');
    if (!$destHandle) {
        fclose($handle);
        return ['error' => 'Error: Unable to open destination file for writing.'];
    }

    stream_copy_to_stream($handle, $destHandle);
    fclose($handle);
    fclose($destHandle);

    return ['success' => true, 'path' => $destPath];
}

if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_FILES['fileToProcess'])) {
    try {
        $result = processFile($_FILES['fileToProcess']);

        // Let the user download the converted file
        if (!empty($result['success'])) {
            // Define path to file
            $file_path = $result['path'];
            // Define file extension
            $file_type = 'application/zip';

            // Send file to user
            header ('Content-Description: File Transfer');
            header ('Content-Type: ' . $file_type);
            header ('Content-Disposition: attachment; filename="' . basename($file_path) . '"');
            header ('Expires: 0');
            header ('Cache-Control: must-revalidate');
            header ('Pragma: public');
            header ('Content-Length: ' . filesize($file_path));

            readfile($file_path);

            // Delete the file
            unlink($file_path);
            exit;
        } else {
            $message = $result['error'];
        }
    } catch (RuntimeException $e) {
        $message = $e->getMessage();
    }
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Chrome Extension Dumper</title>
        <link rel="stylesheet" href="index.css">
        <script src="script.js"></script>
    </head>

    <body>
        <div id="main-container">
            <div id="user-greeting">
                <span id="time-greeting">TherioJunior's Chrome Extension Dumper</span>
            </div>

            <div id="search">
                <div id="search-input-cont">
                    <input id="search-input" placeholder="Enter Chrome Web Store link here" autofocus/>
                </div>

                <div id="search-button-cont">
                    <button class="button" id="search-button" onclick="ripExtension()">Download</button>
                    <button class="button" id="open-store-button" onclick="openStore()">Open Web Store</button>
                </div>
            </div>

            <div id="links">
                <div class="link-group" id="general">
                    <ul>
                        <li class="link-group-title">Information</li>
                        <li><span class="link-text">When you paste a Chrome Web Store link above and hit the download button, a new tab will open and take you to the direct download link of the *.crx file.</span></li>
                    </ul>
                </div>
                <div class="link-group" id="chrome-information">
                    <ul>
                        <li class="link-group-title">Chrome Information</li>
                        <li><span class="link-text" id="version">Latest Chrome version: Loading...</span></li>
                        <li><span class='link-text'>Chrome Release Channel: Stable</span></li>
                    </ul>
                </div>
            </div>

            <div id="links">
                <div class="link-group" id="converter">
                    <ul>
                        <li class="link-group-title">Convert *.crx to *.zip</li>
                        <li><span class='link-text'>Converting the *.crx file to the *.zip format is needed for some archival applications, as Chrome Extensions as of right now feature additional 1321 bytes, which breaks the filetype validation of some archival applications.</span></li>
                        <br>
                        <!--- Leaving an empty space inbetween the "" chars calls for a 'self-post' or whatever, which essentially means that the form will submit a POST request on itself. This is handled via the PHP code at the beginning of the file --->
                        <form action="" method="POST" enctype="multipart/form-data">
                            <input type="file" name="fileToProcess" accept=".crx" class='button' id='file-dialog'>
                            <button type="submit" class='button' id='convert-button'>Process File</button>
                        </form>
                    </ul>
                </div>
            </div>
        </div>
        <p id="version">Loading...</p>
    </body>
</html>

