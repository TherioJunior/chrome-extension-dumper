const apiUrl = 'https://versionhistory.googleapis.com/v1/chrome/platforms/win/channels/stable/versions';
let chromeVersion = '';

function openStore() {
    window.open('https://chromewebstore.google.com/', '_blank');
}

// Call fetchChromeVersion after the website has fully loaded. Prevents website breaking because the 'version' element has not been created yet.
document.addEventListener('DOMContentLoaded', function() {
    fetchChromeVersion();
});

function fetchChromeVersion() {
    const versionElement = document.getElementById('version');

    fetch(apiUrl)
        .then(response => {
            if (!response.ok) {
                throw new Error(`HTTP error! status: ${response.status}`);
            }
            return response.json();
        })
        .then(data => {
            if (data.versions && data.versions.length > 0) {
                const latestVersion = data.versions[0].version;
                versionElement.textContent = versionElement.textContent.replace('Loading...', latestVersion);
                chromeVersion = latestVersion;
            } else {
                versionElement.textContent = 'No version data available';
            }
        })
    .catch(error => {
        console.error('Error fetching data: ', error);
        versionElement.textContent = 'Failed to load version data';
    });
}


function ripExtension() {
    let extensionID = document.getElementById('search-input').value;

    const regex = /\/([a-zA-Z0-9]+)$/;
    const match = extensionID.match(regex);

    if (match && match[1]) {
        extensionID = match[1];
    } else {
        alert('String not found');
        return;
    }

    const downloadURL = `https://clients2.google.com/service/update2/crx?response=redirect&acceptformat=crx2,crx3&prodversion=${chromeVersion}&x=id%3D${extensionID}%26installsource%3Dondemand%26uc`;

    // Open the URL in a new tab or window
    window.open(downloadURL, '_blank');
}
